# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields
from trytond.pyson import Bool, Eval


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    lot_force_assign = fields.Boolean('Lot force assign')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        if cls.lot_required.states.get('required'):
            cls.lot_required.states['required'] |= Bool(
                Eval('lot_force_assign'))
        else:
            cls.lot_required.states['required'] = Bool(
                Eval('lot_force_assign'))
        if 'lot_force_assign' not in cls.lot_required.depends:
            cls.lot_required.depends.append('lot_force_assign')


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
